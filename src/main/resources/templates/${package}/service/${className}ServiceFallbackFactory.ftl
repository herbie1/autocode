package ${package}.service.impl;

import java.util.List;


import feign.hystrix.FallbackFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import ${package}.dao.${className}Dao;
import ${package}.domain.${className};
import ${package}.service.${className}Api;
import ${paginationPackage}.PaginationResult;


/** 
 * Description: ${table.tableDesc}服务
 * Created on ${date}
 * @author  ${author}
 */
@Component
public class ${className}ServiceFallbackFactory implements FallbackFactory<${className}Api> {

    private static final Logger LOGGER = LoggerFactory.getLogger(${className}ServiceFallbackFactory.class);

    @Override
    public ${className}Api create(final Throwable cause) {
    	return new ${className}Api() {

			@Override
    		public ${className}Dto get${className}ById(Long id) {
				${className}ServiceFallbackFactory.LOGGER.error("异常原因: ", cause);
				return null;
			}

			@Override
    		public List<${className}Dto> get${className}ByIds(List<Long> ids) {
				${className}ServiceFallbackFactory.LOGGER.error("异常原因: ", cause);
				return null;
			}

			@Override
        	public PaginationResult<${className}Dto> get${className}ByPage(Integer pageSize, Integer pageIndex) {
				${className}ServiceFallbackFactory.LOGGER.error("异常原因: ", cause);
				return null;
			}

            @Override
            public Boolean add${className}(${className}Dto ${classNameLower}Dto) {
				${className}ServiceFallbackFactory.LOGGER.error("异常原因: ", cause);
                return null;
			}

            @Override
            public Boolean update${className}(${className}Dto ${classNameLower}Dto) {
				${className}ServiceFallbackFactory.LOGGER.error("异常原因: ", cause);
				return null;
            }

            @Override
            public Boolean del${className}ById(Long id) {
				${className}ServiceFallbackFactory.LOGGER.error("异常原因: ", cause);
				return null;
			}


		};
	}
}