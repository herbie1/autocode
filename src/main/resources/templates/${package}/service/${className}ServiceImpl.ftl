package ${package}.service.impl;

import java.util.List;


import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.apache.commons.collections.CollectionUtils;
import feign.Headers;
import org.springframework.web.bind.annotation.*;
import ${package}.dao.${className}Dao;
import ${package}.domain.${className};
import ${package}.service.${className}Api;
import com.netease.edu.util.common.BeanConvertUtils;
import com.netease.edu.util.collection.PaginationResult;
import com.netease.edu.util.collection.PaginationBaseQuery;
import com.netease.edu.commons.utils.sql.SqlBuilder;


/** 
 * Description: ${table.tableDesc}服务实现
 * Created on ${date}
 * @author  ${author}
 */
@Service("${classNameLower}Api")
public class ${className}ServiceImpl implements ${className}Api {
	
	/**
	 * Discription:${table.tableDesc}dao
	 */	
	@Resource
	private ${className}Dao ${classNameLower}Dao;


	@RequestMapping(value = "/api/v1/${appUrl}/get${className}ById", method = RequestMethod.GET)
	public ${className}Dto get${className}ById(@RequestParam(value = "id", required = true)Long id){
		return BeanConvertUtils.safeConvert(${classNameLower}Dao.getById(id), ${className}.class, ${className}Dto.class);
	}

	@RequestMapping(value = "/api/v1/${appUrl}/get${className}ByIds", method = RequestMethod.POST)
	public List<${className}Dto> get${className}ByIds(@RequestBody List<Long> ids){
		if(CollectionUtils.isEmpty(ids)){
			return null;
		}
    	List<${className}> list = ${classNameLower}Dao.getByCondition(SqlBuilder.inSql("id", ids));
        return BeanConvertUtils.safeConvert(list, ${className}.class, ${className}Dto.class);
	}

    @RequestMapping(value = "/api/v1/${appUrl}/get${className}ByPage", method = RequestMethod.GET)
    public ${paginationPackage}.PaginationResult<${className}Dto> get${className}ByPage(@RequestParam(value = "pageSize", required = false) Integer pageSize,
        													@RequestParam(value = "pageIndex", required = false) Integer pageIndex){
		${paginationPackage}.PaginationResult<${className}Dto> pageResult = new ${paginationPackage}.PaginationResult<>();

        PaginationBaseQuery paginationBaseQuery = new PaginationBaseQuery(pageSize, pageIndex, null);
        StringBuilder condition = new StringBuilder(" 1=1 ");
        PaginationResult<${className}> paginationResult = ${classNameLower}Dao.getPaginationByCondition(paginationBaseQuery, condition.toString());
        pageResult.setQuery(BeanConvertUtils.deepSafeConvert(paginationResult.getQuery(), PaginationBaseQuery.class, ${paginationPackage}.PaginationBaseQuery.class));
        if (CollectionUtils.isEmpty(paginationResult.getList())){
        	return pageResult;
        }

        pageResult.setList(BeanConvertUtils.safeConvert(paginationResult.getList(), ${className}.class, ${className}Dto.class));
		return pageResult;
	}

	@RequestMapping(value = "/api/v1/${appUrl}/add${className}", method = RequestMethod.PUT)
	@Headers({ "Content-Type: application/json" })
	public Boolean add${className}(@RequestBody ${className}Dto ${classNameLower}Dto){

		int count = ${classNameLower}Dao.mergeSelective(BeanConvertUtils.safeConvert(${classNameLower}Dto, ${className}Dto.class, ${className}.class));
		if(count > 0){
			return true;
		}
		return false;
	}

	@RequestMapping(value = "/api/v1/${appUrl}/update${className}", method = RequestMethod.PUT)
	@Headers({ "Content-Type: application/json" })
    public Boolean update${className}(@RequestBody ${className}Dto ${classNameLower}Dto){

        int count = ${classNameLower}Dao.mergeSelective(BeanConvertUtils.safeConvert(${classNameLower}Dto, ${className}Dto.class, ${className}.class));
        if(count > 0){
        	return true;
        }
        return false;
	}

	@RequestMapping(value = "/api/v1/${appUrl}/del${className}ById", method = RequestMethod.DELETE)
	@Headers({ "Content-Type: application/json" })
	public Boolean del${className}ById(@RequestParam(value = "id", required = true) Long id){

		boolean flag = ${classNameLower}Dao.deleteById(id);

		return flag;
	}


}