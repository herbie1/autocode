package ${package}.service;

import java.util.List;
import feign.Headers;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;
import ${paginationPackage}.PaginationResult;

import ${package}.domain.${className};

/**
 * Description: ${table.tableDesc}服务
 * Created on ${date}
 * @author  ${author}
 */
@FeignClient(name = "${appName}.${eurekaPostfix}", fallbackFactory = ${className}ServiceFallbackFactory.class)
public interface ${className}Service {

	/**
	* 根据id查询${table.tableDesc}数据
	*/
	@RequestMapping(value = "/api/v1/${appUrl}/get${className}ById", method = RequestMethod.GET)
	public ${className}Dto get${className}ById(@RequestParam(value = "id", required = true)Long id);

	/**
	* 根据ids批量查询${table.tableDesc}数据
	*/
	@RequestMapping(value = "/api/v1/${appUrl}/get${className}ByIds", method = RequestMethod.POST)
	public List<${className}Dto> get${className}ByIds(@RequestBody List<Long> ids);

    /**
    * 分页查询${table.tableDesc}数据
    */
    @RequestMapping(value = "/api/v1/${appUrl}/get${className}ByPage", method = RequestMethod.GET)
    PaginationResult<${className}Dto> get${className}ByPage(@RequestParam(value = "pageSize", required = false) Integer pageSize,
        														@RequestParam(value = "pageIndex", required = false) Integer pageIndex);

    /**
    * 新增一条${table.tableDesc}记录
    */
    @RequestMapping(value = "/api/v1/${appUrl}/add${className}", method = RequestMethod.PUT)
    @Headers({ "Content-Type: application/json" })
    Boolean add${className}(@RequestBody ${className}Dto ${classNameLower}Dto);

	/**
	* 更新一条${table.tableDesc}记录
	*/
	@RequestMapping(value = "/api/v1/${appUrl}/update${className}", method = RequestMethod.PUT)
	@Headers({ "Content-Type: application/json" })
	Boolean update${className}(@RequestBody ${className}Dto ${classNameLower}Dto);

	@RequestMapping(value = "/api/v1/${appUrl}/del${className}ById", method = RequestMethod.DELETE)
	@Headers({ "Content-Type: application/json" })
	public Boolean del${className}ById(@RequestParam(value = "id", required = true) Long id);
	
}
