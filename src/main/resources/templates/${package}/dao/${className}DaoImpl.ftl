package ${package}.dao;

import ${package}.domain.${className};
import ${package}.dao.${className}Dao;
import org.springframework.stereotype.Component;
import com.netease.edu.persist.dao.annotation.DomainMetadata;
import com.netease.edu.persist.dao.sql.BaseDaoSqlImpl;
import com.netease.edu.util.validate.ValidateUtils;

/** 
 * Description: ${table.tableDesc}daoImpl
 * Created on ${date}
 * @author  ${author}
 */
@Component("${classNameLower}Dao")
@DomainMetadata(domainClass= ${className}.class,  tableName="${table.dbName}", idColumn = "id", idProperty = "id")
public class ${className}DaoImpl extends BaseDaoSqlImpl<${className}> implements ${className}Dao {

    @Override
    public int mergeSelective(${className} ${classNameLower}New) {
        ValidateUtils.isTrue(${classNameLower}New != null, "参数异常");
        Long now = System.currentTimeMillis();

        int count = 0;
        ${className} ${classNameLower}Old = null;
        if(${classNameLower}New.getId()!=null) {
            ${classNameLower}Old = this.getById(${classNameLower}New.getId());
        }
        ${classNameLower}New.setGmtModified(now);
        if(${classNameLower}Old == null){
            ${classNameLower}New.setGmtCreate(now);
            count = this.add(${classNameLower}New);
        }else{
            count = this.updateSelectiveById(${classNameLower}New);
        }
        return count;
    }
}
