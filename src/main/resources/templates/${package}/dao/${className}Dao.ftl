package ${package}.dao;

import ${package}.domain.${className};
import com.netease.edu.persist.dao.BaseDao;

/** 
 * Description: ${table.tableDesc}dao
 * Created on ${date}
 * @author  ${author}
 */
public interface ${className}Dao extends BaseDao<${className}> {

    int mergeSelective(${className} ${classNameLower}New);

	
}
