package ${package}.domain;

import java.io.Serializable;
import java.sql.Timestamp;
/** 
 * Description: ${table.tableDesc}Dto
 * Created on ${date}
 * @author  ${author}
 */
public class ${className}Dto  implements Serializable {

	private static final long serialVersionUID = 1L;

<#list table.columns as column>
	/**
     * ${column.label}
     */
	private ${column.type} ${column.name};
</#list>
	
	// setter and getter
<#list table.columns as column>

	public ${column.type} get${column.nameUpper}(){
		return ${column.name};
	}

	public void set${column.nameUpper}(${column.type} ${column.name}){
		this.${column.name} = ${column.name};
	}
</#list>
}
