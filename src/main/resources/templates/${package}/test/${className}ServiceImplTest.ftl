package ${package}.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import org.junit.Test;
import ${package}.domain.${className};
import ${package}.test.common.BaseTestCase;
import ${package}.service.${className}Service;
import com.camelot.openplatform.common.bean.Pager;

/** 
 * Description: ${table.tableDesc} 单元测试类
 * Created on ${date}
 * @author  ${author}
 */
public class ${className}ServiceImplTest extends BaseTestCase{
	@Resource
	private ${className}Service ${classNameLower}Service;

	/**
	 * Discription:${table.tableDesc}分页查询数据
	 * Created on ${date}
	 * @author:${author}
	 */
	@Test
	public void queryPage(){
		List<String> queryFields = new ArrayList<String>();
		queryFields.add("id");
		${className} ${classNameLower} = new ${className}();
		List<${className}> list = ${classNameLower}Service.queryPage(${classNameLower}, new Pager(), null, queryFields);
		for(${className} model: list){
			System.out.println(model.getId());
		}
	}
	
	/**
	 * Discription:${table.tableDesc}查询数据(不分页)
	 * Created on ${date}
	 * @author:${author}
	 */
	@Test
	public void queryList(){
		List<String> queryFields = new ArrayList<String>();
		queryFields.add("id");
		${className} ${classNameLower} = new ${className}();
		List<${className}> list = ${classNameLower}Service.queryList(${classNameLower}, null, queryFields);
		for(${className} model: list){
			System.out.println(model.getId());
		}
	}
	
	/**
	 * Discription:${table.tableDesc}查询总条数
	 * Created on ${date}
	 * @author:${author}
	 */
	@Test
	public void queryCount(){
		${className} ${classNameLower} = new ${className}();
		Long count = ${classNameLower}Service.queryCount(${classNameLower}, null);
		System.out.println(count);
	}
	
	/**
	 * Discription:根据id查询${table.tableDesc}
	 * Created on 2017年02月20日
	 * @author:${author}
	 */
	@Test
	public void queryById(){
		List<String> queryFields = new ArrayList<String>();
		queryFields.add("id");
		${className} model = ${classNameLower}Service.queryById(1l, queryFields);
		System.out.println(model);
	}
	
	/**
	 * Discription:${table.tableDesc}新增
	 * Created on ${date}
	 * @author:${author}
	 */
	@Test
	public void save(){
		${className} ${classNameLower} = new ${className}();
//		${classNameLower}.set
		${classNameLower}Service.save(${classNameLower});
	}
	
	/**
	 * Discription:${table.tableDesc}编辑
	 * Created on ${date}
	 * @author:${author}
	 */
	@Test
	public void edit(){
		${className} ${classNameLower} = new ${className}();
		${classNameLower}Service.edit(${classNameLower});
	}
	
	/**
	 * Discription:${table.tableDesc}单个删除
	 * Created on ${date}
	 * @author:${author}
	 */
	@Test
	public void deleteById(){
		System.out.println(${classNameLower}Service.deleteById(1l));
	}
	
	/**
	 * Discription:${table.tableDesc}批量删除
	 * Created on ${date}
	 * @author:${author}
	 */
	@Test
	public void deleteByIds(){
		List<Long> ids = new ArrayList<Long>();
		ids.add(1l);
		ids.add(2l);
		System.out.println(${classNameLower}Service.deleteByIds(ids));
	}
	
}