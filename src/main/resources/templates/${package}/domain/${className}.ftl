package ${package}.domain;

import java.io.Serializable;
import com.netease.framework.dao.sql.annotation.DataProperty;
import java.sql.Timestamp;
/** 
 * Description: ${table.tableDesc}
 * Created on ${date}
 * @author  ${author}
 */
public class ${className} implements Serializable {

	private static final long serialVersionUID = 1L;

<#list table.columns as column>
	/**
     * ${column.label}
     */
	private ${column.type} ${column.name};
</#list>
	
	// setter and getter
<#list table.columns as column>

    @DataProperty(column = "${column.dbName}")
	public ${column.type} get${column.nameUpper}(){
		return ${column.name};
	}

	public void set${column.nameUpper}(${column.type} ${column.name}){
		this.${column.name} = ${column.name};
	}
</#list>
}
